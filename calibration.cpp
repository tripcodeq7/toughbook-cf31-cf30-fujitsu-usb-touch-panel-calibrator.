/*Panasonic Toughbook Fujitsu Component USB touch Panel calibrator.
 * Tested on CF-31 Formula from 
 * https://wiki.archlinux.org/index.php/Talk:Calibrating_Touchscreen
 * Depends on xinput_calibrator
 *
 * g++ calibration.cpp -o calibrate
 *
 * Made by tripcode!q/7*/

#include<iostream>
#include<fstream>
#include<string>
using namespace std;
//Used for trimming, and converting to int.
int convert(string input){
	string temp="";
	for(int i=0;i<input.size();i++)
		for(int j=0;j<10;j++)
			if(input.at(i)=='0'+j)
				temp.push_back(input.at(i));
	return stoi(temp);
}

int main(){
	//This runs xinput_calibrator and couts it to a file
	system("xinput_calibrator -v > xinCalibrator");
	//This resets the calibration matrix to defaults
	system("xinput set-prop \"Fujitsu Component USB Touch Panel\" \"libinput Calibration Matrix\" 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0");
	ifstream xinFile;
	xinFile.open("xinCalibrator");
	if (!xinFile){
		cout <<  "unable to open xinput calibrator file.\n";
		exit(1);
	}
	//This section is for reading the file
	string input;
	string click_0_X;
	string click_1_Y;
	string click_3_X;
	string click_3_Y;
	char s;
	while (xinFile >> input){
		if(input=="click"){
			xinFile >> input;
			if(input=="1") xinFile >> click_1_Y >> click_1_Y;
			if(input=="3") xinFile >> click_3_X >> click_3_Y;
			if(input=="0") xinFile >> click_0_X;
		}
	}
	//Ask for screen resolution
	int screen_width, screen_height;
	cout << "What is the screen resolution(1024,768?) (CordX,CordY): "; cin >> screen_width >> s >> screen_height; 
	//Runs the calculations
	double a = (screen_width * 0.75) / (convert(click_3_X) - convert(click_0_X));
	double c = ((screen_width / 8.0) - (a * convert(click_0_X))) / screen_width;
	double e = (screen_height * 0.75) / (convert(click_3_Y) - convert(click_1_Y));
	double f = ((screen_height / 8.0) -(e * convert(click_1_Y))) / screen_height;
	//Output text
	cout << "\nRun this to calibrate touchscreen:\n xinput set-prop \"Fujitsu Component USB Touch Panel\" \"libinput Calibration Matrix\" "
	<< a <<", 0.0, "<< c <<", 0.0, "<< e <<", " << f <<", 0.0, 0.0, 1.0"
	<< "\n\nTo Reset:\n xinput set-prop \"Fujitsu Component USB Touch Panel\" \"libinput Calibration Matrix\" 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0\n";
}
